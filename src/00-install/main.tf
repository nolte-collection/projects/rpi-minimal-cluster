module "subnet_addrs" {
  source = "hashicorp/subnets/cidr"

  base_cidr_block = "192.168.0.0/16"
  networks = [
    {
      name     = "client-network"
      new_bits = 8
    },
    {
      name     = "k8s"
      new_bits = 8
    },
  ]
}


module "subnet_addrs_k8s" {
  source = "hashicorp/subnets/cidr"

  base_cidr_block = module.subnet_addrs.network_cidr_blocks["k8s"]
  networks = [
    {
      name     = "metallb"
      new_bits = 4
    }
  ]
}

locals {
  NGINX_ENDPOINT  = cidrhost(module.subnet_addrs_k8s.network_cidr_blocks["metallb"], 1)
  PIHOLE_ENDPOINT = cidrhost(module.subnet_addrs_k8s.network_cidr_blocks["metallb"], 4)
}


module "common_namespaces" {
  source = "git::https://gitlab.com/nolte-collection/tf-modules/k8s-static-data.git//common_namespace"
}


module "metallb" {
  source          = "git::https://gitlab.com/nolte-collection/tf-modules/k8s-metallb.git//modules/install"
  layer2_ip_range = format("%s-%s", cidrhost(module.subnet_addrs_k8s.network_cidr_blocks["metallb"], 1), cidrhost(module.subnet_addrs_k8s.network_cidr_blocks["metallb"], 13))
}

module "ingress_nginx" {
  depends_on = [module.metallb]
  source     = "git::https://gitlab.com/nolte-collection/tf-modules/k8s-nginx-ingress.git//modules/install"
  namespace  = module.common_namespaces.namespace_operators
  lp_ip      = local.NGINX_ENDPOINT

}

module "pihole" {
  depends_on = [module.metallb, module.local_path_provisioner]
  source     = "git::https://gitlab.com/nolte-collection/tf-modules/k8s-pihole.git//modules/install"
  namespace  = "pihole"
  lp_ip      = local.PIHOLE_ENDPOINT
  extra_dns_entries = [
    "address=/k3s.private/${local.NGINX_ENDPOINT}",
  ]
}
