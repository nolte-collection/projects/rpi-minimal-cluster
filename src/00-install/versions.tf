terraform {
  backend "kubernetes" {
    secret_suffix    = "metallb-env"
    load_config_file = true
  }
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.6.2"
    }
    pass = {
      source  = "camptocamp/pass"
      version = "1.4.0"
    }
  }
}
